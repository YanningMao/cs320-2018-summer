(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)
//
extern
fun
{a:t@ype}
insort(xs: list0(a)): list0(a)
//
(* ****** ****** *)

implement
{a}
insort(xs) =
(
case+ xs of
| nil0() => nil0()
| cons0(x, xs) =>
  insord(x, insort<a>(xs)) where
  {
    fun
    insord
    (x0: a, xs: list0(a)): list0(a) =
    (
      case+ xs of
      | nil0() =>
        list0_sing(x0)
      | cons0(x1, xs2) =>
        (
        if
        cmp(x0, x1) <= 0
        then cons0(x0, xs)
        else cons0(x1, insord(x0, xs2))
        ) where
        {
          val cmp = gcompare_val_val<a>
        }
    )
  }
)

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

val xs =
g0ofg1($list{int}(3, 5, 2, 1, 8, 8, 2, 9, 7))
val () = println! ("xs = ", xs)
val xs = println! ("sort(xs) = ", insort<int>(xs))

(* ****** ****** *)

local

implement
gcompare_val_val<int>(x, y) = compare(y, x)

in

val ys =
g0ofg1($list{int}(3, 5, 2, 1, 8, 8, 2, 9, 7))
val () = println! ("ys = ", ys)
val ys = println! ("sort(ys) = ", insort<int>(ys))

end

(* ****** ****** *)

(* end of [insort.dats] *)
