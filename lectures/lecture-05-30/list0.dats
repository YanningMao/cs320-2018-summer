#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

implement main0() = ()

(* ****** ****** *)

(*
datatype
intlist = 
| nil of () // nullary
| cons of (int, intlist) // binary
*)

datatype
list0(a:t@ype) = 
| nil0 of () // nullary
| cons0 of (a, list0(a)) // binary

(* ****** ****** *)

extern
fun
{a:t@ype}
reverse(xs: list0(a)): list0(a)

(* ****** ****** *)

fun
{b:t@ype}
{a:t@ype}
foldleft
(xs: list0(a), res: b, fopr: (b, a) -<cloref1> b): b =
(
case+ xs of
| nil0() => res
| cons0(x, xs) => foldleft<b><a>(xs, fopr(res, x), fopr)
)

fun
{a:t@ype}
{b:t@ype}
foldright
(xs: list0(a), res: b, fopr: (a, b) -<cloref1> b): b =
(
case+ xs of
| nil0() => res
| cons0(x, xs) => fopr(x, foldright<a><b>(xs, res, fopr))
)

(* ****** ****** *)
//
// HX-2018-05-29: not tail-recursive!
//
fun
fromto
(start: int, finish: int): list0(int) =
(
  if
  (start < finish)
  then cons0(start, fromto(start+1, finish))
  else nil0()
)
//
(* ****** ****** *)

(*
val xs = fromto(0, 1000000)
*)

val xs =
$UNSAFE.cast(list0_make_intrange(0, 1000000))

(* ****** ****** *)

(*
fun
{a:t@ype}
length(xs: list0(a)): int =
case xs of
| nil0() => 0 // guarded by the pattern 'nil()'
| cons0(x, xs2) => 1 + length<a>(xs2) // guarded by the pattern 'cons(_, _)
*)

fun
{a:t@ype}
length(xs: list0(a)): int =
foldleft<int><a>(xs, 0, lam(res, x) => res + 1)

(* ****** ****** *)

val () = println! ("length(xs) = ", length<int>(xs))

(* ****** ****** *)

fun
fact(n: int): int = let
//
  val xs =
  $UNSAFE.cast(list0_make_intrange(0, n))
//
in
  foldleft<int><int>(xs, 1, lam(res, x) => res * x)
end

(* ****** ****** *)

fun
{a:t@ype}
append
( xs: list0(a)
, ys: list0(a)): list0(a) =
(
case xs of
| nil0() => ys
| cons0(x, xs) => cons0(x, append<a>(xs, ys))
)

(* ****** ****** *)

val xs2 = append<int>(xs, xs)
val () = println! ("length(xs2) = ", length<int>(xs2))

(* ****** ****** *)

(*
fun
{a:t@ype}
{b:t@ype}
map
( xs: list0(a)
, fopr: a -<cloref1> b): list0(b) =
(
case xs of
| nil0() => nil0()
| cons0(x, xs) => cons0(fopr(x), map<a><b>(xs, fopr))
)
*)
(*
fun
{a:t@ype}
{b:t@ype}
map
( xs: list0(a)
, fopr: a -<cloref1> b): list0(b) =
reverse<b>
(
foldleft<list0(b)><a>(xs, nil0(), lam(res, x) => cons0(fopr(x), res))
)
*)
fun
{a:t@ype}
{b:t@ype}
map
( xs: list0(a)
, fopr: a -<cloref1> b): list0(b) =
(
foldright<a><list0(b)>(xs, nil0(), lam(x, res) => cons0(fopr(x), res))
)

(* ****** ****** *)

val ys = map<int><string>(xs, lam(x) => itoa(x))

(* ****** ****** *)

(*
val zs = map<string><int>(ys, lam(y) => (println!(y); 0))
*)

(* ****** ****** *)

fun
{a:t@ype}
append
(xs: list0(a), ys: list0(a)): list0(a) =
foldright(xs, ys, lam(x, res) => cons0(x, res))

(* ****** ****** *)

(*
fun
reverse
(xs: intlist): intlist =
(
case xs of
| nil() => nil()
| cons(x, xs) => extend(reverse(xs), x)
)
*)

(* ****** ****** *)

fun
{a:t@ype}
revappend
(xs: list0(a), ys: list0(a)): list0(a) =
foldleft(xs, ys, lam(res, x) => cons0(x, res))

(*
fun
{a:t@ype}
reverse
(xs: list0(a)): list0(a) = revappend<a>(xs, nil0())
*)

(* ****** ****** *)

fun
{a:t@ype}
foreach
( xs: list0(a)
, fwork: (a) -<cloref1> void): void =
ignoret
(
foldleft(xs, 0, lam(_, x) => (fwork(x); 0))
)

fun
{a:t@ype}
rforeach
( xs: list0(a)
, fwork: (a) -<cloref1> void): void =
ignoret
(
foldright(xs, 0, lam(x, _) => (fwork(x); 0))
)

(* ****** ****** *)
