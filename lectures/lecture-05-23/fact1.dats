(* ****** ****** *)
(*
fact(n) = 1 * 2 * ... * n
*)
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

(*
extern
fun fact : int -> int
implement
fact(n) =
if n > 0 then n * fact(n-1) else 1
*)
fun
fact(n: int): int =
if
n > 0
then let
  val n1 = n - 1
(*
  val () = println! ("n1 = ", n1)
*)
  val res1 = fact(n1)
(*
  val () = println! ("res1 = ", res1)
*)
in
  n * res1
end // end of [then]
else 1 // end of [else]

(* ****** ****** *)

(*
implement
main0() =
println!("fact(10) = ", fact(10))
*)
implement
main0(argc, argv) = let
//
val inp =
(
if argc > 1
  then g0string2int(argv[1]) else 10
) : int
//
in
  println! ("fact(", inp, ") =\n", fact(inp))
end // end of [main0]

(* ****** ****** *)

(* end of [fact1.dats] *)
