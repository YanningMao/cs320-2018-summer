#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

//we always need this to make c happy
implement main0() = ()

//lab doc availible at https://docs.google.com/document/d/1ZhQ9WaGmO4PcUEwCmTV2O9WoBpFA8l1yVWTdDxRyIfc/edit?usp=sharing


fun is_even(n:int):bool =
 if n = 0
 then true
 else if n = 1
      then false
      else is_even(n-2)

val () = println!(is_even(8))
val () = println!(is_even(2000000))

fun is_even(n:int):bool =
 if n = 0
 then true
 else ~ is_even(n-1)

//val () = println!(is_even(2000000))
val () = println!(is_even(8))


fun prime2(i:int,k:int,is_prime:bool):bool = //return new is_prime
  if k < i
  then //recursion
      if i % k = 0
      then false // prime2(i, k+1, false) //is_prime = False
      else prime2(i, k+1, is_prime)//k += 1
  else is_prime

fun prime1(i:int,n:int, big_prime:int): int =
  if i < n
  then let
         val is_prime = prime2(i,2,true)
       in
         if is_prime
         then prime1(i+1,n,i)
         else prime1(i+1,n,big_prime)
       end
  else big_prime

fun biggest_prime(n:int):int = prime1(1,n,1)

val () = println!(biggest_prime(100))





fun biggest_prime(n:int):int =
  let
    fun prime1(i:int, big_prime:int): int =
      if i < n
      then let
             val is_prime =
               let
                 fun prime2(k:int,is_prime:bool):bool = //return new is_prime
                   if k < i
                   then //recursion
                       if i % k = 0
                       then false // prime2(i, k+1, false) //is_prime = False
                       else prime2(k+1, is_prime)//k += 1
                   else is_prime
               in
                 prime2(2,true)
               end
           in
             if is_prime
             then prime1(i+1,i)
             else prime1(i+1,big_prime)
           end
      else big_prime
  in
    prime1(1,1)
  end


val () = println!(biggest_prime(100))


//fun repeat_twice(f: int -> int): int -> int = lam n => f(f(n))
//
//fun square(n: int): int = (n*n)