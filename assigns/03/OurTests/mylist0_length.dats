#include "./../MySolution/assign03_sol.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests

val () = println!()
val () = println!("Testing on an empty list.\n\n Result should be 0\n Actual result is ", mylist0_length(nil0()))
val () = println!()
val () = println!()
val () = println!("Testing on a list with 1 element\n\n Result should be 1\n Actual result is ", mylist0_length(list0_make_sing(8)))
val () = println!()
val () = println!()
val () = println!("Testing on a list with 5 element\n\n Result should be 5\n Actual result is ", mylist0_length(g0ofg1($list{int}(~5, 3, 2147483647, 11111, ~327641))))
val () = println!()
val () = println!()
val () = println!("Testing on a list with 50000 element\n\n Result should be 50000\n Actual result is ", mylist0_length(list0_make_intrange(1, 50001)))
val () = println!()
