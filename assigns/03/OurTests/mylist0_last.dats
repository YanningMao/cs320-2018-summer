#include "./../MySolution/assign03_sol.dats"
// add your tests at https://bitbucket.org/marklemay/cs320-2018-summer-tests

// Does not need to test on an empty list.
val () = println!()
val () = println!("Testing on (2)\n\n Result should be 2\n Actual result is ", mylist0_last(list0_make_sing(2)))
val () = println!()
val () = println!()
val () = println!("Testing on range(213214)\n\n Result should be 213213\n Actual result is ", mylist0_last(list0_make_intrange(1, 213214)))
val () = println!()
