(* ****** ****** *)
//
// HX:
// How to compile:
// myatscc assign04_sol.dats
//
// How to test it:
// ./assign04_sol_dats
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

#include "./../assign04.dats"

(* ****** ****** *)

(*
Please implement
the following functions:

fun // 5 points // successor
succ_intrep(intrep): intrep

fun // 5 points // addition
add_intrep_intrep(intrep, intrep): intrep

fun
mul_int_intrep(int, intrep): intrep
fun // 5 points // multiplication
mul_intrep_intrep(intrep, intrep): intrep

fun // 5 points
intrep_make_int(int): intrep // for non-negative int

fun print_intrep(intrep): int
*)

(* ****** ****** *)

(* end of [assign04_sol.dats] *)
